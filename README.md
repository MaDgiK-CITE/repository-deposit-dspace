# Using DSpace repository with Argos

The repository-deposit-dspace module implements the [https://code-repo.d4science.org/MaDgiK-CITE/repository-deposit-base](https://) interface for the DSpace repository.

## Setup

After creating the jar from the project, environment variables should be set since they are used in the application.properties
1) STORAGE_TMP_DSPACE - a temporary storage needed
2) CONFIGURATION_DSPACE - path to json file which includes the configuration for the repository

### JSON configuration file

The following fields should be set:<br>
**depositType** - an integer representing how the dmp user can deposit in the repository,<br>
a. **0** stands for system deposition meaning the dmp is deposited using argos credentials to the repository,<br>
b. **1** stands for user deposition in which the argos user specifies his/her own credentials to the repository,<br>
c. **2** stands for both ways deposition if the repository allows the deposits of dmps to be made from both argos and users accounts<br>
note: depositType should be set to **0** since dspace does not provide oauth2 protocol but, instead, uses system accounts<br>
**repositoryId** - unique identifier for the repository<br>
**email** - system email provided for the depositions<br>
**password** - system password provided for the depositions<br>
**workflowEmail** - email(can be the same as the system email if that user is privileged) provided for the workflows of dspace<br>
**workflowPassword** - password provided for the workflows of dspace<br>
**repositoryUrl** - repository's api url e.g. "https://api7.dspace.org/server/api/"<br>
**repositoryRecordUrl** - repository's record url, this url is used to index dmps that are created e.g. "https://demo7.dspace.org/handle/"<br>
**collection** - collection uuid in which all dmps that are deposited will be resided<br>
**hasLogo** - if the repository has a logo<br>