package eu.eudat.depositinterface.dspacerepository.config;

import java.util.List;

public interface ConfigLoader {
    byte[] getLogo(String repositoryId);
    List<DSpaceConfig> getDSpaceConfig();
}
