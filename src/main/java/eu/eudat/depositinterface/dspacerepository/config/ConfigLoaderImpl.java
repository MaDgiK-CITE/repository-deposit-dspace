package eu.eudat.depositinterface.dspacerepository.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service("dspaceConfigLoader")
public class ConfigLoaderImpl implements ConfigLoader{
    private static final Logger logger = LoggerFactory.getLogger(ConfigLoaderImpl.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    private List<DSpaceConfig> dSpaceConfigs = new ArrayList<>();

    private final Environment environment;

    @Autowired
    public ConfigLoaderImpl(Environment environment){
        this.environment = environment;
    }

    @Override
    public List<DSpaceConfig> getDSpaceConfig() {
        if(dSpaceConfigs == null || dSpaceConfigs.isEmpty()){
            try{
                dSpaceConfigs = mapper.readValue(getStreamFromPath(environment.getProperty("configuration.dspace")), new TypeReference<List<DSpaceConfig>>() {});
            } catch (IOException e) {
                logger.error(e.getLocalizedMessage(), e);
            }
        }
        return dSpaceConfigs;
    }

    @Override
    public byte[] getLogo(String repositoryId) {
        if (!dSpaceConfigs.isEmpty()) {
            DSpaceConfig dSpaceConfig = dSpaceConfigs.stream().filter(x -> x.getRepositoryId().equals(repositoryId)).findFirst().orElse(null);
            if (dSpaceConfig != null) {
                String logo = dSpaceConfig.getLogo();
                InputStream logoStream;
                if(logo != null && !logo.isEmpty()) {
                    logoStream = getStreamFromPath(logo);
                }
                else{
                    logoStream = getClass().getClassLoader().getResourceAsStream("dspace.svg");
                }
                try {
                    return (logoStream != null) ? logoStream.readAllBytes() : null;
                }
                catch (IOException e){
                    logger.error(e.getMessage(), e);
                }
            }
            return null;
        }
        return null;
    }

    private InputStream getStreamFromPath(String filePath) {
        try {
            return new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            logger.info("loading from classpath");
            return getClass().getClassLoader().getResourceAsStream(filePath);
        }
    }
}
