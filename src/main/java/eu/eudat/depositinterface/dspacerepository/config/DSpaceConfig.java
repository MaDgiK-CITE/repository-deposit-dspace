package eu.eudat.depositinterface.dspacerepository.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.eudat.depositinterface.repository.RepositoryDepositConfiguration;

public class DSpaceConfig {
    private enum DepositType {
        SystemDeposit(0), UserDeposit(1), BothWaysDeposit(2);

        private final int value;

        DepositType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static DepositType fromInteger(int value) {
            switch (value) {
                case 0:
                    return SystemDeposit;
                case 1:
                    return UserDeposit;
                case 2:
                    return BothWaysDeposit;
                default:
                    throw new RuntimeException("Unsupported Deposit Type");
            }
        }
    }

    @JsonProperty("depositType")
    private int depositType;
    @JsonProperty("repositoryId")
    private String repositoryId;
    @JsonProperty("email")
    private String email;
    @JsonProperty("password")
    private String password;
    @JsonProperty("workflowEmail")
    private String workflowEmail;
    @JsonProperty("workflowPassword")
    private String workflowPassword;
    @JsonProperty("repositoryUrl")
    private String repositoryUrl;
    @JsonProperty("repositoryRecordUrl")
    private String repositoryRecordUrl;
    @JsonProperty("collection")
    private String collection;
    @JsonProperty("hasLogo")
    private boolean hasLogo;
    @JsonProperty("logo")
    private String logo;

    public int getDepositType() {
        return depositType;
    }
    public void setDepositType(int depositType) {
        this.depositType = depositType;
    }

    public String getRepositoryId() {
        return repositoryId;
    }
    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getWorkflowEmail() {
        return workflowEmail;
    }
    public void setWorkflowEmail(String workflowEmail) {
        this.workflowEmail = workflowEmail;
    }

    public String getWorkflowPassword() {
        return workflowPassword;
    }
    public void setWorkflowPassword(String workflowPassword) {
        this.workflowPassword = workflowPassword;
    }

    public String getRepositoryUrl() {
        return repositoryUrl;
    }
    public void setRepositoryUrl(String repositoryUrl) {
        this.repositoryUrl = repositoryUrl;
    }

    public String getRepositoryRecordUrl() {
        return repositoryRecordUrl;
    }
    public void setRepositoryRecordUrl(String repositoryRecordUrl) {
        this.repositoryRecordUrl = repositoryRecordUrl;
    }

    public String getCollection() {
        return collection;
    }
    public void setCollection(String collection) {
        this.collection = collection;
    }

    public boolean isHasLogo() {
        return hasLogo;
    }
    public void setHasLogo(boolean hasLogo) {
        this.hasLogo = hasLogo;
    }

    public String getLogo() {
        return logo;
    }
    public void setLogo(String logo) {
        this.logo = logo;
    }

    public RepositoryDepositConfiguration toRepoConfig() {
        RepositoryDepositConfiguration config = new RepositoryDepositConfiguration();
        config.setDepositType(this.depositType);
        config.setRepositoryId(this.repositoryId);
        config.setRepositoryUrl(this.repositoryUrl);
        config.setRepositoryRecordUrl(this.repositoryRecordUrl);
        config.setHasLogo(this.hasLogo);
        return config;
    }
}
