package eu.eudat.depositinterface.dspacerepository.interfaces;

public class PatchBooleanEntity {

    private String op;
    private String path;
    private boolean value;

    public String getOp() {
        return op;
    }
    public void setOp(String op) {
        this.op = op;
    }

    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }

    public boolean getValue() {
        return value;
    }
    public void setValue(boolean value) {
        this.value = value;
    }
}
