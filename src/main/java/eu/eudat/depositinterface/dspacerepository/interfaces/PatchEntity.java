package eu.eudat.depositinterface.dspacerepository.interfaces;

import java.util.List;
import java.util.Map;

public class PatchEntity {

    private String op;
    private String path;
    private List<Map<String, String>> value;

    public String getOp() {
        return op;
    }
    public void setOp(String op) {
        this.op = op;
    }

    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }

    public List<Map<String, String>> getValue() {
        return value;
    }
    public void setValue(List<Map<String, String>> value) {
        this.value = value;
    }
}
